# README #

### Resumen ###

* Prueba tecnica realizada para Sistemas OEE
* Version 1.0.0

### Despliegue ###

* Clonar el repositorio
* Para ejecutar el frontend, en la ruta '/src/frontend/Prueba-tecnica' ejecutar el comando npm install.
* Cuando termine el proceso ejecutar el comando ng serve
* Para usar el modo de datos persistentes se espera que exista un servidor PostgreSQL con las siguientes caracteristicas:
* El servidor se debe estar ejecutnado en la maquina local en el puerto por defecto(5432).
* Debe exisitir una base de datos con el nombre 'postgres' las credenciales seran, nombre 'postgres' y contaseña 'post'

### Contato ###

Creado por Alexandru Alexandrov (alexandr.alexandrov.92@gmail.com)