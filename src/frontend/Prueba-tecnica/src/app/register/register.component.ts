import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService } from './../services/user-service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  public form: FormGroup;

  constructor(    
    private fb: FormBuilder,
    private toast: ToastrService,
    private userService: UserService,
    private router: Router
  ) {
    this.form = this.fb.group({
      name: [null, Validators.required],
      age: [null, Validators.required],
      email: [null, Validators.required],
      password: [null, Validators.required]
    });
  }

  public createUser(): void {
    this.userService.AddUser(
      this.form.controls['name'].value,
      this.form.controls['age'].value,
      this.form.controls['email'].value,
      this.form.controls['password'].value).subscribe(
        data => {
          if(data) {
            this.toast.success("User created.");
            this.router.navigateByUrl("/login");
          }
          else {
            this.toast.error("User already exists.");
          }
        },
        err => {},
        () => {}
      );
  }

}