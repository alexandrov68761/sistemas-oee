import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

export abstract class BaseService {

    protected httpOptions = {
        headers: new HttpHeaders({
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json; charset=utf-8'
        }),
        params: new HttpParams()
    };

    constructor(
        protected http: HttpClient)
        {
            this.http = http;
        }
}