import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from './base.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseService {

  private url: string;

  constructor(
    http: HttpClient) {

      super(http);
      this.url = environment.APIEndpoint;

    }     

    // GET
    GetUsers() {
      const url = this.url + '/users';

      return this.http
        .get(url, this.httpOptions);
    }

     // POST
    AddUser(name: string, age: number, email: string, password: string) {
      const url = this.url + '/user';

      const payload = {
        name: name,
        age: age,
        email: email,
        password: password
      };

      return this.http
       .post(url, payload, this.httpOptions);
    }

    Login(email: string, password: string) {
      const url = this.url + '/login';

      const payload = {
        name: null,
        age: null,
        email: email,
        password: password
      };

      return this.http
       .post(url, payload, this.httpOptions);
     }
}