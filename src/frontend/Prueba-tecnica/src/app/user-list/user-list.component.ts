import { Component, OnInit } from '@angular/core';

import { UserService } from './../services/user-service.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  public users: any;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userService.GetUsers().subscribe( (data) => {
      this.users = data;
    })
  }

}