import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService } from './../services/user-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private toast: ToastrService,
    private userService: UserService,
    private router: Router) { 
      this.form = this.fb.group({
        name: [null, [Validators.required, Validators.email]],
        password: [null, Validators.required]
      })

    }

  public login(): void {
    if(!this.form.controls['name'].value) {
      this.toast.error("Email cannot be blank.");
      localStorage.clear();
      return;
    }

    if(!this.form.controls['name'].valid) {
      this.toast.error("Wrong email format.");
      localStorage.clear();
      return;
    }

    if(!this.form.controls['password'].valid) {
      this.toast.error("Password cannot be blank.");
      localStorage.clear();
      return;
    }

    this.userService.Login(this.form.controls['name'].value, this.form.controls['password'].value).subscribe( 
      data => {
        if(data == -1) {
          this.toast.error("User don't exists or wrong password.");
          localStorage.clear();
        }
        else{
          this.toast.success("Logged in.");
          localStorage.setItem('loggedin', "true");
          this.router.navigateByUrl("/user-list");
        }
      },
      err => {},
      () => {}
    );
  }
}