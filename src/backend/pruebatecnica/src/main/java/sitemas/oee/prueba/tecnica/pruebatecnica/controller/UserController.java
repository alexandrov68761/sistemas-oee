package sitemas.oee.prueba.tecnica.pruebatecnica.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import sitemas.oee.prueba.tecnica.pruebatecnica.dao.UserDAO;
import sitemas.oee.prueba.tecnica.pruebatecnica.dao.UserRepository;
import sitemas.oee.prueba.tecnica.pruebatecnica.entities.User;
import sitemas.oee.prueba.tecnica.pruebatecnica.security.PasswordAuthentication;

@RestController
public class UserController {
    
    @Autowired
    UserRepository repository;
    private UserDAO userDAO = new UserDAO();

    @Value("${persistence}")
    private boolean persistence;

    public UserDAO getUserDAO() {
        return this.userDAO;
    }

    public void setUserDao(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @CrossOrigin
    @GetMapping("/users")
    public List<User> getUsers() {
        List<User> userList;

        if(this.persistence) {
            userList = repository.findAll();
        }
        else {
            userList = userDAO.getAllUsers().getUserList();
        }

        return userList;
    }

    @CrossOrigin
    @PostMapping("/user")
    public boolean addUser(@RequestBody User user) {
        List<User> userList;
        PasswordAuthentication pass = new PasswordAuthentication();
        String password = pass.hash(user.getPassword());
        
        if(this.persistence) {
            userList = repository.findAll();
            if(userList.contains(user)) {
                return false;
            }

            this.repository.save(
                new User(
                    user.getName(),
                    user.getEmail(),
                    password,
                    user.getAge()
                )
            );
        }
        else {
            userList = userDAO.getAllUsers().getUserList();
            if(userList.contains(user)) {
                return false;
            }
            int id = userDAO
                .getAllUsers()
                .getUserList()
                .size()
                + 1;
            
            user.setId(id);
            user.setPassword(password);
            userDAO.addUser(user);
        }

        return true;
    }

    @CrossOrigin
    @PostMapping("/login")
    public int login(@RequestBody User user) {
        
        List<User> userList;

        if(this.persistence) {
            userList = this.repository.findAll();
        }
        else { 
            userList = this.userDAO.getAllUsers().getUserList();
        }

        if(userList.indexOf(user) != -1) {
            User user2 = userList.get(userList.indexOf(user));

            PasswordAuthentication pass = new PasswordAuthentication();
            if(pass.authenticate(user.getPassword(), user2.getPassword())) {
                return 1;
            }
            else{
                return -1;
            }
        }

        return -1;
    }
}