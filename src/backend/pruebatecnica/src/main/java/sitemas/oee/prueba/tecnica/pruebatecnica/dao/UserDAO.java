package sitemas.oee.prueba.tecnica.pruebatecnica.dao;

import sitemas.oee.prueba.tecnica.pruebatecnica.entities.User;
import sitemas.oee.prueba.tecnica.pruebatecnica.entities.Users;

public class UserDAO {
    
    private static Users users = new Users();

    public Users getAllUsers() {
        return users;
    }

    public void addUser(User user) {
        users.getUserList().add(user);
    }
}
