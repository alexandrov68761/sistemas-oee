package sitemas.oee.prueba.tecnica.pruebatecnica.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import sitemas.oee.prueba.tecnica.pruebatecnica.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findAll();
}
